from pyo import Server, Sine
import time

# Start the sound engine
s = Server().boot().start()

# Create a sine wave
tone = Sine()

# Create a modulator wave
modulator = Sine()

# Set modulator frequency
modulator.freq = 1

# Connect modulator to carrier frequency
tone.freq = 440 + (220 * modulator)

# Connect sine wave to system output
tone.out()

# let sound play
time.sleep(6.0)
