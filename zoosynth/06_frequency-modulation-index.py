from pyo import Server, Sine
import time

# Start the sound engine
s = Server().boot().start()

# Create a sine wave
tone = Sine()

# Create a modulator sine wave
modulator = Sine()

# Set modulator frequency
modulator.freq = 120.0;

# Create an LFO for the modulation index
modulationIndexLFO = Sine();
modulationIndexLFO.freq = 0.5;

# Connect to modulator to the carrier frequency
tone.freq = 440 + (modulationIndexLFO * 700 * modulator);

# Connect sine wave to system output
tone.out()

# let the sound play
time.sleep(4.0)