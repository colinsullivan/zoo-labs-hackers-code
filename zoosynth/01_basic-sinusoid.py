from pyo import Server, Sine
import time

# Start the sound engine
s = Server().boot().start()

# Create a sine wave
tone = Sine()

# Connect sine wave to system output
tone.out()

# let the sound play
time.sleep(4.0)