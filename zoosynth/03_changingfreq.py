from pyo import Server, Sine, Fader
import time

# Start the sound engine
s = Server().boot().start()

# Create the amplitude envelope
envelope = Fader(fadein=0.2, fadeout=1.0, dur=2.0)

# Create a sine wave
tone = Sine()

# fade in / out the sine wave (apply the envelope)
smoothTone = tone * envelope

# Connect smoothed sine wave to system output
smoothTone.out()

# start the envelope
envelope.play()

# let the sound play
time.sleep(4.0)