import time

half_step=2.0**(1.0/12)

#for step in (0,1,2,3,4,5,6,7,8,9,10,11,12):
#   print  step, 440*half_step**step
   
def compute_freq_step(base_freq, steps):
	return base_freq * half_step**steps
	
middle_c = compute_freq_step(440, -9)
note_steps = {"C" : 0, "D" : 2, "E" : 4, "F" : 5, "G" : 7, "A" : 9, "B" : 11 }

def note_to_freq_simple(note):
	return compute_freq_step(middle_c, note_steps[note])

def play_song(tempo, song):
	rest = 60.0 / tempo # rest between notes in seconds
	for note in song.upper().split():
		print note, note_to_freq_simple(note)
		time.sleep(rest)
		
tempo = 80
song = "c C G G A A G"
play_song(tempo, song)