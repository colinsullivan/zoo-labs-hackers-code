from pyo import Server, Sine, Fader
import time
import random

# Start the sound engine
s = Server().boot().start()

# Create the amplitude envelope
envelope = Fader(fadein=0.001, fadeout=0.001, dur=0.002)

# Create a sine wave
tone = Sine()

# fade in / out the sine wave (apply the envelope)
smoothTone = tone * envelope

# Connect smoothed sine wave to system output
smoothTone.out()



while True:
	# set the frequency of the sine wave
	tone.freq = random.randrange(220, 1760)

	# start the envelope
	envelope.play()

	# let the sound play
	time.sleep(0.002)
