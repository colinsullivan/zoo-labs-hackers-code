f = open("/Users/david/Desktop/rappers_delight_lyrics.txt", "r")
print f  # what's f? it's a file.

lyrics = f.read()  # read everything from f, as a string, into a new variable called "lyrics"

print lyrics # print out the lyrics

print len(lyrics) # how long are the lyrics?

print len  # what's len? it's a function!

# print out some shorter strings
print len("hello world")
print len("david")
print len("david")
print len("   ")
print len("") # the empty string

# tuples
print (1, 2, 3, 4, 5)

# a
a = (8,3,1,78,45)
print a
print sorted(a)
print sorted(a, reverse=True)

# an outtake on variable assignment
a = 42
print a
a = a
print a  # nothing changed

b = 7
a = 42
a = b
print a  # a has the same value as b


# for loops!

# prints out each value
for i in (8,3,1,78,45):
  print i
  
  
# indentation matters!  this program is different
for i in (8,3,1,78,45):
  print i
  print "hello"

# than this one
for i in (8,3,1,78,45):
  print i
print "hello"

# the square of each number
for i in (8,3,1,78,45):
   print i * i

# split into words
print lyrics.split()

for word in lyrics.split():
   print word
   
print len( lyrics.split() )
len("hello")

for c in "hello":
  print c
  
print len( ("cat", "dog", "mouse") )

for i in range(100000):
  print i
  
# indexing
s = "hello world"
print s[4]  # the 4th letter? No! the 3rd. indexing starts at 0

print s[3]  # that's the 4th
print s[0]  # that's the 1st letter

print lyrics.split()[3]  # here's the 4th WORD of the lyrics
lyrics[3]  # here's the 4th LETTER of the lyrics

words = lyrics.split()
print len(lyrics)  # lyrics is a STRING with 14191 characters.
print len(words)   # words is a LIST with 2878 strings.
# BUT, len() and indexing work the same on both!

print type(lyrics)  # verify that lyrics is a string
print type(words)  # verify that words is a list


