# Review of string things

s = "hello world it's a nice day" # create a string, name it s

print s

print len(s) # how long is s?

# indexing
print s[0] # the first character
print s[3] # the 4th character

print s[0:3]  # the first 3 characters

# index ranges
print s[6:11] # the 7th through the 11th characters

# how to get the LAST character
print s[ len(s)-1 ] # here's one way!

print s[-1]  # here's a better way

print s[-3]  # the 3rd  from the end

print s[2:-3]  # you can do ranges with negative numbers as well

# splitting
print s.split()

x = s.split()

# sorting
print sorted(x)  # get a sorted version of x
print sorted( (5,6,7,1,2,3,6) )  # get a sorted version of these numbers
print sorted(s)  # you can even sort a string 

# string formatting
print "%d + %d is %d" % (4, 5, 9)  # each %d gets replaced with one of the numbers

print "%d + %d is %d" % (4, 5, 19) # look ma! no math.

print "%d + %d is %s" % (4, 5, "buffalo")  # use %s for strings

# error: too many arguments
print "%d + %d is %s" % (4, 5, "buffalo", 8)  # need the same number of %'s and args

# error: type mismatch
print "%d + %d is %d %s" % (4, 5, "buffalo", 8) # %s for strings, %d for integers



print "%f degrees" % (400.57)  # by default floating point numbers get 5 decimal points
#400.570000 degrees

print "%f degrees" % (0.0)
#0.000000 degrees

print "%.1f degrees" % (400.57)  # only 1 decimal point
#400.6 degrees

print "%.4f degrees" % (400.57) # 4 decimal points
#400.5700 degrees

print "%4d monkeys" % (37)
#  37 monkeys

print "%04d monkeys" % (37)
#0037 monkeys

print "%014d monkeys" % (37)
#00000000000037 monkeys

print "%04d monkeys" % (1923)
#1923 monkeys

print "%04d monkeys" % (923)
#0923 monkeys

print "%04d monkeys" % (23)
#0023 monkeys

print "%04d monkeys" % (3)
#0003 monkeys

y = "%d %s like %s" % (14, "cats", "dogs")
print y
print len(y)

y = "%d %s like %s" % (14, "catfish", "dogs")
print y
print len(y)

# error
print "she said "hello" to me"

print "she said \"hello\" to me" # we need to escape the quotes

print 'she said "hello" to me'  # or you can put the double quote in single quotes

# error
print 'dogs dog's dogs''  # same issue using single quotes
print 'dogs dog\'s dogs\''

print "go\nto\nwork" # \n means a NEW LINE




























